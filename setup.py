#!/usr/bin/env python

import setuptools
from distutils.core import setup

setup(name='neurokernel-lif-example',
      version='1.0',
      packages=['lif_example',
                'lif_example.neurons',
                'lif_example.synapses',
		'lif_example.utils',
                'lif_example.models'],

      install_requires=[
        'configobj >= 5.0.0',
        'neurokernel >= 0.1',
      ]
     )
